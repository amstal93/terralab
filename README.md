# Template to create new GitLab projects using Terraform.

## Step 1

- Log in to GitLab.
- In the upper-right corner, click your avatar and select Settings.
- On the User Settings menu, select Access Tokens.
- Choose a name and optional expiry date for the token.
- Choose the desired scopes.
- Click the Create personal access token button.
- Save the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again.

## Step 2

Edit `main.tf` to set up your project the way you want.

## Step 3

- Set your Terraform variables in the matching `variables` file of a project.
  - Be sure to set your token variable!

## Step 4

In your terminal:

- `terraform init` -- Init Terraform project.
- `terraform validate` -- Make sure your config is valid.
- `terraform apply` -- Build.

---
Credits:

<a target="_blank" href="https://icons8.com/icons/set/test-tube-1">Test Tube</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>